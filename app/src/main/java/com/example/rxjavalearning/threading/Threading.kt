package com.example.rxjavalearning.threading

import com.example.rxjavalearning.ready.mapThreadName
import android.annotation.SuppressLint
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/* Schedulers
In RxJava you cannot play with threads as you wish. We need to access threads through built in schedulers.
If task needs any particular thread we need to be able to pick the right scheduler. That is the trickiest part -
Choosing right scheduler for the job that we need.

1. Schedulers.io()
 This scheduler has a thread-pool for non-CPU intensive I/O operations.
 We can draw as many threads from it as we need "hypothetically speaking" because threads are not infinite resource.
 However what this means is that scheduler is not capped in size and threadpool can grow as needed.

2. Schedulers.computation()
 This scheduler is used for CPU intesnsive work like processing large data sets, image handling and processing etc.
 Unlike schedulers.io() this thread has capped threadpool with size up to the number of processors available.
 As this scheduler is only suitable for high intensity CPU tasks many of threads from this thread-pool would possibly lead to starvation

3. AndroidSchedulers.mainThread
 Special scheduler that is not available in core RxJava library - hence we use RxAndroid extension library
 This is used for android apps to tell the system to use mainThread to do specific operations.

4. Schedulers.newThread() creates completely new thread  to perform our tasks on and it does so each time it is used.
 This should be used sparingly and if you know what you're doing since creating and tearing down threads is an expensive operation

5. Schedulers.single() used for performing tasks in a sequential manner

6. Schedulers.from(executor: Executor) can be used to create custom Scheduler with your own executor

SubscribeOn/ObserveOn
 */

object Multithreading  {
    @SuppressLint("CheckResult")
    fun emitSubscribeSchedulersIO() {
        Observable.just(1, 2, 3, 4)
            .subscribeOn(Schedulers.io())
            .doOnNext { println("Emitting $it on: ${Thread.currentThread().name.mapThreadName()}") }
            .subscribe { println("Consuming $it on: ${Thread.currentThread().name.mapThreadName()}") }
    }


    @SuppressLint("CheckResult")
    fun getIntegersFromApi() {
        Observable.just(1, 2, 3, 4, 5)
            .doOnNext { println("Emitting $it on: ${Thread.currentThread().name.mapThreadName()}") }
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { println("Consuming $it on: ${Thread.currentThread().name.mapThreadName()}") }
    }

    @SuppressLint("CheckResult")
    fun getIntegersFromAPiComplex() {
        Observable.just(1, 2, 3, 4, 5)
            .doOnNext { println("Emitting $it on: ${Thread.currentThread().name.mapThreadName()}") }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.computation())
            .map {
                println("Mapping happening on: ${Thread.currentThread().name.mapThreadName()}")
                it * it
            }
            .observeOn(Schedulers.newThread())
            .filter {
                println("Filtering happening on: ${Thread.currentThread().name.mapThreadName()}")
                (it % 2) == 0
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { println("Consuming $it on: ${Thread.currentThread().name.mapThreadName()}") }
    }
}