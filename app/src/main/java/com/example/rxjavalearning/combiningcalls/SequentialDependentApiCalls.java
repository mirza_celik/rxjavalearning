package com.example.rxjavalearning.combiningcalls;

import com.example.rxjavalearning.api.Api;
import com.example.rxjavalearning.api.data.Comment;
import com.example.rxjavalearning.api.data.FullPostingObject;
import com.example.rxjavalearning.api.data.Posting;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableSource;
import io.reactivex.Single;
import io.reactivex.functions.Function;

public final class SequentialDependentApiCalls {

    // Purpose: combining multiple API calls to get single usable data structure
    // {
    // Posting1 {
    // List<Comment> comments
    // },
    // Posting2 {
    // List<Comment> comments
    // }
    // ...
    //
    //List<FullPostingObject>
    //
    // }
    // Each Post has list of comments
    // Why Java?

    public static Single<List<FullPostingObject>> getAllPostingsObjects() {
        return Api
                .INSTANCE
                .getService()
                .getPostsRx()
                .flatMapIterable(new Function<List<Posting>, List<Posting>>() {
                    @Override
                    public List<Posting> apply(List<Posting> postings) throws Exception {
                        return postings;
                    }
                })
                .flatMap(new Function<Posting, ObservableSource<FullPostingObject>>() {
                    @Override
                    public ObservableSource<FullPostingObject> apply(Posting posting) throws Exception {
                        return Api
                                .INSTANCE
                                .getService()
                                .getPostComments(posting.getId())
                                .flatMapObservable(new Function<List<Comment>, ObservableSource<FullPostingObject>>() {
                                    @Override
                                    public ObservableSource<FullPostingObject> apply(List<Comment> comments) throws Exception {
                                        FullPostingObject fullPostingObject = new FullPostingObject();
                                        fullPostingObject.setPosting(posting);
                                        fullPostingObject.setPostingComments(comments);
                                        return Observable.just(fullPostingObject);
                                    }
                                });
                    }
                })
                .toList();
    }
}
