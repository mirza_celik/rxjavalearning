package com.example.rxjavalearning.combiningcalls;

import com.example.rxjavalearning.api.Api;
import com.example.rxjavalearning.api.data.Album;
import com.example.rxjavalearning.api.data.Photo;
import com.example.rxjavalearning.api.data.Todo;
import com.example.rxjavalearning.ready.Phone;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.functions.Function6;
import io.reactivex.schedulers.Schedulers;
import kotlin.Triple;

// Multiple independent API calls - might e.g. for home screen different features
public class SequentialIndependentApiCalls {

    public static Single<Triple<Integer, Integer, Integer>> getMultipleIndependentEndpoints() {
        Single<List<Photo>> photosSingle = Api.INSTANCE.getService().getPhotos().subscribeOn(Schedulers.io());
        Single<List<Photo>> photosSingle2 = Api.INSTANCE.getService().getPhotos().subscribeOn(Schedulers.io());

        Single<List<Todo>> todoSingle = Api.INSTANCE.getService().getTodos().subscribeOn(Schedulers.io());
        Single<List<Todo>> todoSingle2 = Api.INSTANCE.getService().getTodos().subscribeOn(Schedulers.io());

        Single<List<Album>> albumsSingle = Api.INSTANCE.getService().getAlbums().subscribeOn(Schedulers.io());
        Single<List<Album>> albumsSingle2 = Api.INSTANCE.getService().getAlbums().subscribeOn(Schedulers.io());

        return Single.zip(photosSingle, photosSingle2, todoSingle, todoSingle2, albumsSingle, albumsSingle2, new Function6<List<Photo>, List<Photo>, List<Todo>, List<Todo>, List<Album>, List<Album>, Triple<Integer, Integer, Integer>>() {
            @Override
            public Triple<Integer, Integer, Integer> apply(List<Photo> photos, List<Photo> photos2, List<Todo> todos, List<Todo> todos2, List<Album> albums, List<Album> albums2) throws Exception {
                return new Triple<>(photos.size() + photos2.size(), albums.size() + albums2.size(), todos.size() + todos2.size());
            }
        });
    }
}