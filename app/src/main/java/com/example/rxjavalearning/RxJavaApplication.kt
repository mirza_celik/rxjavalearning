package com.example.rxjavalearning

import android.app.Application
import android.util.Log
import io.reactivex.exceptions.UndeliverableException
import io.reactivex.plugins.RxJavaPlugins
import java.io.IOException
import java.lang.IllegalArgumentException
import java.lang.IllegalStateException
import java.lang.NullPointerException
import java.net.SocketException

class RxJavaApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        setRxJavaErrorHandler()
    }

    private fun setRxJavaErrorHandler() {
        RxJavaPlugins.setErrorHandler {e ->
            var error = e
            if (error is UndeliverableException) {
                error = error.cause
            }

            if ((error is IOException) || (error is SocketException)) {
                // fine, irrelevant network problem or API that throws on cancelation
                return@setErrorHandler
            }

            if (error is InterruptedException) {
                // fine, some blocking code was interrupted by a dispose call
                return@setErrorHandler
            }

            if ((error is NullPointerException) || (error is IllegalArgumentException)) {
                Thread.currentThread().uncaughtExceptionHandler?.uncaughtException(Thread.currentThread(), error)
                return@setErrorHandler
            }

            if (error is IllegalStateException) {
                Thread.currentThread().uncaughtExceptionHandler?.uncaughtException(Thread.currentThread(), error)
                return@setErrorHandler
            }
        }
    }
}