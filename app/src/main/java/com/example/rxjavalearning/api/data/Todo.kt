package com.example.rxjavalearning.api.data

data class Todo(
    val completed: Boolean?,
    val id: Int?,
    val title: String?,
    val userId: Int?
)