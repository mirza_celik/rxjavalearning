package com.example.rxjavalearning.api.data

data class Posting(
    val userId: Int,
    val id: Int,
    val title: String,
    val body: String
)

data class Comment(
    val postId: Int,
    val id: Int,
    val name: String,
    val email: String,
    val body: String
)

data class Message(
    val id: Int,
    val body: String,
    val title: String,
    val userId: Int
)

data class Person(
    val firstName: String,
    val lastName: String,
    val age: Int
)

class FullPostingObject {
    var posting: Posting? = null
    var postingComments: List<Comment>? = null
}