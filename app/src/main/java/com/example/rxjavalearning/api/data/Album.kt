package com.example.rxjavalearning.api.data

data class Album(
    val id: Int?,
    val title: String?,
    val userId: Int?
)