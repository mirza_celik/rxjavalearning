package com.example.rxjavalearning.api.data

data class Geo(
    val lat: String?,
    val lng: String?
)