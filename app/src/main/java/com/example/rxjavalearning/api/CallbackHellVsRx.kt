package com.example.rxjavalearning.api

import com.example.rxjavalearning.api.data.Posting
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

object CallbackHellVsRx {

    private fun startApiRetrofitCall() {
        Api.service
            .getPosts()
            .enqueue(object : Callback<List<Posting>> {
                override fun onFailure(call: Call<List<Posting>>, t: Throwable) {
                    println("Retrofit all posts call has failed >>>${t.localizedMessage}<<<")
                }

                override fun onResponse(
                    call: Call<List<Posting>>,
                    response: Response<List<Posting>>
                ) {
                    val posting = response.body()?.get(0)
                    println("Retrofit all posts call success >>>$posting <<<")

                    posting?.let {
                        Api.service.getPost(it.id)
                            .enqueue(object : Callback<Posting> {
                                override fun onFailure(call: Call<Posting>, t: Throwable) {
                                    println("Retrofit posts call has failed >>>${t.localizedMessage}<<<")
                                }

                                override fun onResponse(
                                    call: Call<Posting>,
                                    response: Response<Posting>
                                ) {
                                    println("Retrofit post call success >>>${response.body()?.title}<<<")
                                }
                            })
                    }
                }
            })
    }

    private fun startApiRxJavaCall() {
        Api.service
            .getPostsRx()
            .map { it[0] }
            .flatMapSingle {
                Api.service.getPostRx(it.id)
            }
            .subscribeOn(Schedulers.io())
            .subscribe(object : Observer<Posting> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(t: Posting) {

                }

                override fun onError(e: Throwable) {

                }

            })
    }
}