package com.example.rxjavalearning.api

import com.example.rxjavalearning.api.data.*
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import io.reactivex.Observable
import io.reactivex.Single
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

object Api {

    interface JsonPlaceHolderService {
        @GET("posts/{id}")
        fun getPost(@Path("id") id: Int): Call<Posting>

        @GET("posts")
        fun getPosts(): Call<List<Posting>>

        @GET("posts/{id}")
        fun getPostRx(@Path("id") id: Int): Single<Posting>

        @GET("posts")
        fun getPostsRx(): Observable<List<Posting>>

        @GET("posts/{id}/comments")
        fun getPostComments(@Path("id") id: Int): Single<List<Comment>>

        @GET("users")
        fun getUsers(): Single<List<User>>

        @GET("todos")
        fun getTodos(): Single<List<Todo>>

        @GET("albums")
        fun getAlbums(): Single<List<Album>>

        @GET("photos")
        fun getPhotos(): Single<List<Photo>>
    }

    private var retrofit = Retrofit
        .Builder()
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl("https://jsonplaceholder.typicode.com/")
        .build()

    var service: JsonPlaceHolderService = retrofit.create(JsonPlaceHolderService::class.java)
}