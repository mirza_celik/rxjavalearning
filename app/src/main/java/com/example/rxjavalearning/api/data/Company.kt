package com.example.rxjavalearning.api.data

data class Company(
    val bs: String?,
    val catchPhrase: String?,
    val name: String?
)