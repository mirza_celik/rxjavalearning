package com.example.rxjavalearning

import DeferExample.showcaseDefer
import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import com.example.rxjavalearning.api.data.FullPostingObject
import com.example.rxjavalearning.combiningcalls.SequentialDependentApiCalls.getAllPostingsObjects
import com.example.rxjavalearning.combiningcalls.SequentialIndependentApiCalls
import com.example.rxjavalearning.concatvsmerge.CombineArrays.concatTwoArrays
import com.example.rxjavalearning.ready.Multithreading.getIntegersFromAPiComplex
import com.example.rxjavalearning.ready.mapThreadName
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class MainActivity : BaseActivity() {

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Observable.just(1, 2, 3, 4)
            .doOnNext { println("Emitting $it on: ${Thread.currentThread().name.mapThreadName()}") }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.computation())
            .map {
                println("Mapping $it on ${Thread.currentThread().name.mapThreadName()}")
                it * 2
            }
            .observeOn(Schedulers.newThread())
            .filter {
                println("Filter $it on ${Thread.currentThread().name.mapThreadName()}")
                (it % 2) == 0
            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe { println("Consuming $it on ${Thread.currentThread().name.mapThreadName()}")}
    }
}