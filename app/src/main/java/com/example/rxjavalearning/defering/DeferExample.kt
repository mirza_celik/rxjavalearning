import io.reactivex.Observable

object DeferExample {

    fun showcaseDefer() {
        val phone = Phone()
        val observable = phone.getObservable()
        val deferredObservable = phone.getDeferredObservable()

        phone.model = "Android"

        observable.subscribe {
            println("NonDeferred $it")
        }

        deferredObservable.subscribe {
            println("Deferred $it")
        }
    }


    class Phone {
        var model = "iPhone"

        fun getObservable() = Observable.just(model)
        fun getDeferredObservable() = Observable.defer {
            Observable.just(model)
        }
    }
}