package com.example.rxjavalearning

import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import io.reactivex.Observer
import io.reactivex.SingleObserver
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

open class BaseActivity : AppCompatActivity() {
    private val bag = CompositeDisposable()

    abstract inner class BaseObserver<T>: Observer<T> {
        override fun onComplete() {
            println("onComplete")
        }

        override fun onSubscribe(d: Disposable) {
            println("onSubscribe ${d.isDisposed}")
            addRxSubscription(d)
        }

        override fun onError(e: Throwable) {
            Log.e(this::class.java.simpleName, e.localizedMessage!!)
        }
    }

    abstract inner class BaseSingleObserver<T>: SingleObserver<T> {
        override fun onError(e: Throwable) {
            println(e.localizedMessage)
        }

        override fun onSubscribe(d: Disposable) {
            addRxSubscription(d)
        }
    }

    fun addRxSubscription(d: Disposable) {
        bag.add(d)
    }

    override fun onDestroy() {
        super.onDestroy()
        bag.clear()
        println(bag.size() == 0)
    }
}