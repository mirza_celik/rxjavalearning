package com.example.rxjavalearning.concatvsmerge

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

// What finishes first? Why in each case?

val list1Observable = Observable.fromIterable(1 .. 10).delay(1000.toLong(), TimeUnit.MILLISECONDS)
val list2Observable = Observable.fromIterable((10 .. 20)).delay(500.toLong(), TimeUnit.MILLISECONDS)


object CombineArrays {
    fun concatTwoArrays() {
        Observable.merge(
            list1Observable,
            list2Observable
        )
            .subscribe(object : Observer<Int> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {
                    println("Merge")
                }

                override fun onNext(t: Int) {
                    println(t)
                }

                override fun onError(e: Throwable) {

                }
            })
    }
}

