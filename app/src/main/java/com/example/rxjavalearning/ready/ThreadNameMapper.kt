package com.example.rxjavalearning.ready

fun String.mapThreadName(): String {
    return when(this) {
        "RxCachedThreadScheduler-1" -> "SCHEDULERS.IO"
        "RxComputationThreadPool-1" -> "SCHEDULERS.COMPUTATION"
        "RxNewThreadScheduler-1" -> "SCHEDULERS.NEWTHREAD"
        "main" -> "ANDROID.MAIN"
        else -> ""
    }
}