package com.example.rxjavalearning.ready

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import java.util.concurrent.TimeUnit

val list1Observable = Observable.fromIterable(1 .. 10).delay(1000.toLong(), TimeUnit.MILLISECONDS)

val list2Observable = Observable.fromIterable((10 .. 20)).delay(500.toLong(), TimeUnit.MILLISECONDS)

object ConcatArrays {

    fun concatArrays() {
        Observable.concat(
            list1Observable,
            list2Observable
        )
            .subscribe(object : Observer<Any> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {
                    println("Concatenation")
                }

                override fun onNext(value: Any) {
                    println(value)
                }

                override fun onError(e: Throwable) {

                }
            })
    }
}


object MergeArrays {
    fun mergeArrays() {
        Observable.merge(
            list1Observable,
            list2Observable
        )
            .subscribe(object : Observer<Any> {
                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable) {
                    println("Merge")
                }

                override fun onNext(value: Any) {
                    println(value)
                }

                override fun onError(e: Throwable) {
                }

            })
    }
}