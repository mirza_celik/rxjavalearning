package com.example.rxjavalearning.observable_types

import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable

object ObservableType {

    // May emit one or more values - with the implication it may return more than 1 - hence we have onNext(t: T)
    fun getNumbers(): Observable<Int> {
        return Observable
            .fromArray(1, 2, 3, 4, 5)
    }

    // Usecase
    private fun startObservable() {
        ObservableType
            .getNumbers()
            .subscribe(object : Observer<Int> {
                override fun onComplete() {

                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onNext(t: Int) {

                }

                override fun onError(e: Throwable) {

                }
            })
    }
}

