package com.example.rxjavalearning.observable_types

import io.reactivex.Single
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable

object SingleType {
    private val names = listOf("name1", "name2", "name3")

    // Suitable for use cases where we expect a single value to be returned. E.g. Network call, database query
    fun getListOfNames(): Single<List<String>> {
        return Single.fromCallable {

            Thread.sleep(2000) //doing an API call simulation
            names
        }
    }

    // Use case
    private fun startSingle() {
        val oldMilis = System.currentTimeMillis()
        SingleType
            .getListOfNames()
            .subscribe(object : SingleObserver<List<String>> {
                override fun onSuccess(t: List<String>) {
                    val newMillis = System.currentTimeMillis()
                    val diffInSec = (newMillis - oldMilis) / 1000
                    println(diffInSec)
                    println(t)
                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onError(e: Throwable) {

                }
            })
    }
}