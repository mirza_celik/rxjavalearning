package com.example.rxjavalearning.observable_types

import io.reactivex.Maybe
import io.reactivex.MaybeObserver
import io.reactivex.disposables.Disposable
import java.lang.IllegalArgumentException

object MaybeType {
    private val names = listOf("name1", "name2", "name3")

    // Maybe is very Similar to combination of Completable and Single
    // We have to determine 3 different states for Maybe
    // Was it successful or it errored out. If it was successful does it have result or not
    fun getMaybe(success: Boolean, hasResult: Boolean): Maybe<List<String>> {
        return Maybe.create { maybe ->

            if (success) {
                if (hasResult) {
                    maybe.onSuccess(names)
                } else {
                    maybe.onComplete()
                }
            } else {
                maybe.onError(IllegalArgumentException("Fake error"))
            }
        }
    }

    // Use case
    private fun startMaybe() {
        MaybeType
            .getMaybe(false, false)
            .subscribe(object : MaybeObserver<List<String>> {
                override fun onSuccess(t: List<String>) {
                    println(t)
                }

                override fun onComplete() {
                    println("onComplete")
                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onError(e: Throwable) {
                    println("onError")
                }
            })
    }
}