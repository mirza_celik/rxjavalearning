package com.example.rxjavalearning.observable_types

import io.reactivex.Completable
import io.reactivex.CompletableObserver
import io.reactivex.disposables.Disposable
import java.lang.IllegalArgumentException

object CompletableType {
    private const val success = false

    // Similar to a Single but does not return a result
    // Suitable for scenarios where we need Success/Failure information only.
    // E.g. POST or PUT calls
    fun createCompletable(): Completable {
        return Completable
            .fromCallable {
                Thread.sleep(2000)
                if (success) {
                    Completable.complete()
                } else{
                    Completable.error(IllegalArgumentException("FakeException"))
                }
            }
    }

    // use case
    private fun startCompletable() {
        CompletableType
            .createCompletable()
            .subscribe(object : CompletableObserver {
                override fun onComplete() {
                    println("onCOmplete")
                }

                override fun onSubscribe(d: Disposable) {

                }

                override fun onError(e: Throwable) {
                    println("onError")
                }
            })
    }
}