package com.example.rxjavalearning.observable_types

import io.reactivex.disposables.Disposable
import io.reactivex.subjects.BehaviorSubject

object Subjects {

    fun startSubjects(): Disposable {

        val behaviorSubject = BehaviorSubject.createDefault(10)

        val disposable =  behaviorSubject.subscribe ({ newValue ->
            println("onNext: $newValue")
        }, { error ->
            println("onError ${error.localizedMessage}")
        }, {
            println("onComplete")
        })

        behaviorSubject.onNext(1)
        behaviorSubject.onNext(2)
        disposable.dispose()
        behaviorSubject.onNext(3)
        behaviorSubject.onNext(4)
        behaviorSubject.onNext(5)

        return disposable
    }
}